#!/usr/bin/env python
'''
A simple GUI for creating Ledger transactions.
To do:
- dropdowns for Payees, Accounts
'''
import PySimpleGUI as sg
from pydatum import Datum
import pyperclip

class App():
    def __init__(self):
        self.window = None

    def create_layout(self):
        today = Datum()
        account_box_size = (35, 1)
        amount_box_size = (12, 1)
        currency_box_size = (4, 1)

        layout = [
            # Date Payee
            [sg.Text("Date", size=(11, 1)), sg.Text("Payee")],
            [sg.In(today.get_iso_date_string(), size=(11,1), key='date_input'),
                sg.CalendarButton('Cal', target='date_input', key='date',
                    default_date_m_d_y=(today.get_month(), today.get_day(), today.get_year())),
                sg.InputText(key='payee_input', size=(50, 1), focus=True)],

            # Description
            [sg.Text("Note"), sg.InputText(key='note_input')],

            # Split rows
            [sg.Text("account", size=(25, 1)), sg.Text("amount", size=(10, 1)), sg.Text('Currency')],
            [sg.InputText(key='account1_input', size=account_box_size), 
                sg.InputText(key='amount1_input', size=amount_box_size),
                sg.Input(key='currency1_input', size=currency_box_size)],
            [sg.InputText(key='account2_input', size=account_box_size),
                sg.InputText(key='amount2_input', size=amount_box_size),
                sg.Input(key='currency1_input', size=currency_box_size)],

            # test dropdown
            [sg.InputCombo(['choice 1', 'choice 2'], key="account1")],

            # todo: button for more rows
            [sg.Button('+', key='add_rows', size=(20, 2)),
            sg.Button('load_accounts', key='load_accounts_button')],

            [sg.Button('Create')],

            [sg.Multiline('blah', key='out_text', size=(50, 20), disabled=True),
                sg.Button('Copy')]
        ]

        return layout

    def run(self):
        ''' run the app '''
        layout = self.create_layout()    
        self.window = sg.Window('New Transaction', 
            size=(500, 300),
            default_element_size=(40, 1), 
            return_keyboard_events=True,
            grab_anywhere=False).Layout(layout)

        while True:     # Event Loop
            event, values = self.window.Read()
            if event is None:
                break

            if event == 'Create':
                self.on_Create_click(values)
            
            if event == 'Copy':
                # copy to clipboard
                content = values['out_text']
                self.copy_to_clipboard(content)

            if event == 'load_accounts_button':
                self.load_accounts()

            print(event)

    def create_ledger_entry(self, values):
        ''' Create text entry '''

        date = values['date_input'].strip()
        payee = values['payee_input']
        note = values['note_input']
        account1 = values['account1_input']
        amount1 = values['amount1_input']
        account2 = values['account2_input']
        amount2 = values['amount2_input']

        output = f"{date} {payee}\n"

        if (note):
            output += f"{note}"
        output += f"    {account1}    {amount1}\n"
        output += f"    {account2}    {amount2}\n"

        return output

    def on_Create_click(self, values):
        ''' Create the transaction '''
        # Create the transaction with the values
        # Add it to the text box.
        output = self.create_ledger_entry(values)
        
        self.window.FindElement('out_text').Update(disabled=False)
        self.window.FindElement('out_text').Update(output)
        #self.window.Refresh()
        self.window.FindElement('out_text').Update(disabled=True)

    def copy_to_clipboard(self, content):
        import pyperclip

        pyperclip.copy(content)

    def load_accounts(self):
        ''' load accounts from ledger '''
        import subprocess
        result = subprocess.run(['ledger'], capture_output=True)
        if result.stdout:
            print(result.stdout)
        if result.stderr:
            print(result.stderr)


###############################
if __name__ == "__main__":
    app = App()
    app.run()
