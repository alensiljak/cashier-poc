# NCurses

NCurses implementation of cashier.

- [Windows Libs](http://www.lfd.uci.edu/~gohlke/pythonlibs/#curses)
- [NPyscreen Demo](https://www.youtube.com/watch?v=niV_7xxkVQg)
- [NPyScreen Docs](https://npyscreen.readthedocs.io/)
