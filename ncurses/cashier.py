#!/usr/bin/env python3
# encoding: utf-8
'''
NCurses implementation of cashier.
The data entry form for ledger/beancount.
Goals: multiplatform (Windows, Linux, Android/Termux). Use existing applications to provide data.
'''
import npyscreen


class DataModel():
    ''' The data model for the application '''
    def __init__(self):
        self.accounts = []


class LedgerRunner():
    ''' Invokes ledger to provide background data '''

    def __init__(self, data_path):
        self.book = data_path

    def balance(self):
        ''' return balance '''
        command = "bal"
        self.ledger(command)

    def ledger(self, command):
        ''' Invoke the ledger binary. It has to be in the path. '''
        import subprocess
        subprocess.run([
            "ledger",
            "-f", self.book,
            command
        ], check=True)


class EntryForm(npyscreen.Form):
    ''' Form for entering new transactions '''

    def create(self):
        # todo: load accounts?
        self.name = "Cashier - data entry for ledger"

        self.date = self.add(npyscreen.TitleDateCombo, name="Date:")
        self.payee = self.add(npyscreen.TitleText, name="Payee:",)

        #self.entries: npyscreen.TitleMultiLine = self.add(npyscreen.TitleMultiLine, name = "Entries")
        #self.entries = []
        self.entries = self.add(npyscreen.MultiLineEditable, name = "Entries")
        #self.entries.add_line(0, 0, "test")
        #default_entries = self.create_default_entries()
        # self.entries.set_values(default_entries)

        value_list = [
           "This is the first",
           "This is the second",
           "This is the third",
           "This is the fourth",
        ]
        t = self.add(npyscreen.MultiLineEditableBoxed,
                        max_height=20,
                        name='List of Values',
                        footer="Press i or o to insert values", 
                        values=value_list, 
                        slow_scroll=False)

        self.add(npyscreen.ButtonPress, name="Balance",
                 when_pressed_function=self.balance)

    # def activate(self):
    #     self.edit()
    #     self.parentApp.setNextForm(None)

    def afterEditing(self):
        # print the transaction in ledger format
        tx = self.create_ledger_transaction(
            self.date.get_value(), self.payee.get_value())
        print(tx)

        self.parentApp.setNextForm(None)

    def create_default_entries(self):
        ''' Create default entries for the posting '''
        #first = npyscreen.TextfieldUnicode()
        #second = npyscreen.Textfield()
        #return [first, second]
        pass

    def balance(self):
        print("balance!")

    def create_ledger_transaction(self, date, payee="", postings=[]):
        ''' Format the data in ledger transaction syntax '''
        output = f"{date} {payee}\n"

        for posting in postings:
            output += f"{posting.account}    {posting.amount} {posting.currency}"

        return output


class App(npyscreen.NPSAppManaged):
    def onStart(self):
        # form = npyscreen.Form(name = "Cashier - data entry for ledger")
        form = EntryForm(minimum_lines=20, minimum_columns=30)

        # balance_button =

        # form.edit()
        #self.addForm("MAIN", EntryForm)
        self.registerForm("MAIN", form)
        # self.setNextForm(None)


if __name__ == "__main__":
    app = App()
    app.run()
