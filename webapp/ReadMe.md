# Cashier Web App

Web application, base content for the distributions:

- mobile (Cordova/PhoneGap)
- desktop (Electron)

## Packaging

Run with `parces index.html` or `parces index.html --open` to open the browser.
The script is in "npm run-script run".

Get the contents of **dist** folder into www folder of cordova.

## To-Do

- Check components at https://vuejsexamples.com/
