/*
    Posting entity.
*/

export default class Posting {
    constructor() {
        this.account = ""
        this.amount = ""
        this.currency = ""
    }
};